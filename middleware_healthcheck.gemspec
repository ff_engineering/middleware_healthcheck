# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'middleware_healthcheck/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'middleware_healthcheck'
  s.version     = MiddlewareHealthcheck::VERSION
  s.authors     = ['Adam Wieczorkowski', 'Claudio Perez Gamayo', 'Jan Wieczorkowski']
  s.email       = 'devops@firefield.com'
  s.homepage    = 'https://bitbucket.org/ff_engineering/middleware_healthcheck/src/master/'
  s.summary     = 'Rack middleware to provide a healthcheck endpoint.'
  s.description = 'Rack middleware to provide a healthcheck endpoint, useful for load balancers health checks.'
  s.license     = 'MIT'

  s.files         = ['lib/middleware_healthcheck.rb']
  s.files        += Dir['lib/**/*.rb']
  s.files        += Dir['[A-Z]*'] + Dir['spec/**/*']
  s.test_files    = s.files.grep(%r{^spec/})

  s.add_dependency 'rails', '>= 4.0'

  s.add_development_dependency 'pry-byebug', '~> 3.4'
  s.add_development_dependency 'rspec-rails', '~> 3.5'
  s.add_development_dependency 'sqlite3', '~> 1.3'
end
