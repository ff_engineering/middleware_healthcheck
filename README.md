[![MiddlewareHealthcheck](logo.png)]()

# MiddlewareHealthcheck

[![CircleCI](https://circleci.com/bb/ff_engineering/middleware_healthcheck.svg?style=svg)](https://circleci.com/bb/ff_engineering/middleware_healthcheck) [![Gem Version](https://img.shields.io/gem/v/middleware_healthcheck.svg?style=flat-square)](https://rubygems.org/gems/middleware_healthcheck) [![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](MIT-LICENSE)

Customizable health-check API endpoint for your service.



# Overview

Add a health check API endpoint (e.g. HTTP /healthcheck) that returns the health of your service to your rack or Ruby on Rails application.

## Features

- customizable endpoint
- custom checks
- two default checks provided

## Default checks

The default check when no params are passed doesn't try to connect to the database. This is useful when you want to check the health of an application instance inside of a load balancer for example (even if the database is down, the application servers would be healthy and should not be replaced).

```
/healthcheck
```

There is another check included that takes the database into consideration and will fail if the database is down.

```
/healthcheck?checks=active_record
```

# Getting started

## Installation
Add this line to your application's Gemfile:
```ruby
gem 'middleware_healthcheck'
```

And then execute:
```bash
$ bundle install
```

Or install it yourself as:
```bash
$ gem install middleware_healthcheck
```

# Usage
Run basic check (without running any advanced checkers):
```
/healthcheck
```

To run all available checkers, add `full` parameter:
```
/healthcheck?full=1
```

To run selected checkers, add `checks` parameter, where the value is the name of checkers separated by a comma:
```
/healthcheck?checks=active_record,second_checker,another_checker
```

To change default path and parameters see Configuration section

## Configuration
Create ``config/initializers/middleware_healthcheck.rb`` file
```ruby
MiddlewareHealthcheck.configure do |config|
  config.healthcheck_path = "my_custom_path"
  ...
end
```
Available options:

- healthcheck_path - the url endpoint for the healthcheck API (default: 'healthcheck')
- full_check_param_name (default: 'full')
- selected_check_param_name (default: 'checks')
- error_response_status
- success_response_status
- success_response_body
- errors_delimiter
- selected_check_param_split_delimiter


## Custom Checkers
Your Custom Checker class should look like this:

```ruby
class MyCustomChecker
  attr_accessor :error

  def initialize(_app, _env)
  end

  def healthy?
    if everything_ok?
      true
    else
      self.error = 'Error message'
      false
    end
  end
end
```

To include Custom Checker, just add

```ruby
HealthcheckMiddleware.configure do |config|
  config.checkers << MyCustomChecker
end
```
in initializer.

# Contributing

Contributors:
- Adam Wieczorkowski adam.wieczorkowski@naturaily.com
- Claudio Perez Gamayo claudio@firefield.com
- Jan Wieczorkowski jan.wieczorkowski@naturaily.com

Use the provided dockerized development environment. For more information check the [CONTRIBUTING](CONTRIBUTING.md) file.

## Development

```
# build the docker containers
docker-compose build

# run the specs
docker-compose run --rm app bundle exec rspec

```

# License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

