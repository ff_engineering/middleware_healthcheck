# frozen_string_literal: true

module MiddlewareHealthcheck
  module DefaultCheckers
    class ActiveRecordChecker
      NOT_CONNECTED_ERROR = "Can't connect to database."
      EXCEPTION_REGEXP = /^ActiveRecord::/.freeze

      attr_accessor :error

      def initialize(_app, _env); end

      def healthy?
        ActiveRecord::Base.establish_connection
        ActiveRecord::Base.connection
        if ActiveRecord::Base.connected?
          true
        else
          self.error = NOT_CONNECTED_ERROR
          false
        end
      rescue StandardError => e
        if e.class.to_s.match? EXCEPTION_REGEXP # rubocop:disable Style/GuardClause
          self.error = e.message
          false
        else
          raise e
        end
      end
    end
  end
end
