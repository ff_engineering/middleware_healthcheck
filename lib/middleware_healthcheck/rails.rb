# frozen_string_literal: true

module MiddlewareHealthcheck
  class Railtie < Rails::Railtie
    initializer 'middleware_healthcheck.configure_rails_initialization' do
      app.middleware.insert_before ActionDispatch::ShowExceptions, MiddlewareHealthcheck::Middleware
    end

    def app
      Rails.application
    end
  end
end
