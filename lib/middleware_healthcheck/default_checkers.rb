# frozen_string_literal: true

require 'middleware_healthcheck/default_checkers/active_record_checker' if defined? ActiveRecord::Railtie

module MiddlewareHealthcheck
  module DefaultCheckers
  end
end
