# Changelog

## [Unreleased]

## [1.0.1] - 2019-04-04
### Changed
- Change middleware hook for Rails to insert before ActionDispatch::ShowExceptions

## [1.0.0] - 2019-04-03

### Added
- Add Rubocop and address various offences
- Add CHANGELOG.md and CODE_OF_CONDUCT.md
- Add contributors in README.md

### Fixed
- Fix gemspec's gem dependencies

## [0.2.2] - 2019-04-02
### Changed
- Update docs

### Fixed
- Fix gemspec

## [0.2.1] - 2019-03-21
### Added
- Dockerize project
- Add more docs

## [0.2.0] - 2017-03-08
### Added
- Add basic specs
- Add gem configuration feature
- User can add custom checkers

### Changed
- Update gems
- DB connection checked by default

## [0.1.0] - 2017-02-16
### Added
- Initial files for the gem
